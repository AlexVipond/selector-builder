const baleada = require('@baleada/tailwind-theme')

module.exports = {
  purge: false,
  theme: {
    ...baleada,
    purge: false,
    customForms: theme => ({
      default: {
        input: {
          backgroundColor: 'transparent',
        },
        select: {
          backgroundColor: 'transparent',
        },
      },
    })
  },
  // variants: baleada.variants,
  plugins: [
    require('@tailwindcss/custom-forms')
  ]
}