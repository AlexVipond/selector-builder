const purgecss = require('@fullhuman/postcss-purgecss')({
  // Specify the paths to all of the template files in your project
  content: [
    './src/index.html',
    './src/**/*.jsx',
    './src/extracted/css/**/*.js',
  ],
  whitelist: [
    'text-indigo-90',
    'text-indigo-10',

    'border-indigo-40',
    'border-gray-40',

    'bg-gray-60',
    'bg-indigo-60',
    'text-gray-60',
    'text-indigo-60',

    'border-indigo-20',
    'border-gray-20',
    'bg-indigo-20',
    'bg-gray-20',

    'border-indigo-90',
  ],
  // Include any special characters you're using in this regular expression
  defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
})

module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
    ...(process.env.NODE_ENV == 'production' ? [purgecss] : []),
  ],
}