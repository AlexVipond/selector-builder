const babel = require('rollup-plugin-babel')

module.exports = {
  alias: {
    react: '@pika/react',
    'react-dom': '@pika/react-dom'
  },
  rollupInputOptions: {
    plugins: [
      babel({
        exclude: 'node_modules',
      }),
    ]
  }
}