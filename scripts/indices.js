const { generateIndex } = require('@baleada/prepare')

function indices () {
  generateIndex('./src/extracted/js')
  generateIndex('./src/extracted/js/selector-classes')
  generateIndex('./src/extracted/js/selector-classes/util')
  
  generateIndex('./src/extracted/json')

  generateIndex('./src/extracted/jsx', { extensions: ['jsx'] })

  generateIndex('./src/extracted/css/composed')
}

indices()