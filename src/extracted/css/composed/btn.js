// Button styles
export default '\
inline-flex \
items-center \
\
px-2 \
py-1 \
\
border-4 \
rounded-full \
\
transition \
duration-3 \
\
transform \
hover:scale-110 \
focus:scale-110 \
active:scale-100 \
'