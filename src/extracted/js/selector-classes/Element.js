import Selector from './Selector'
import { toTraversalArrow, isRawTraversal, wrapTraversal } from './util'

export default class Element extends Selector {
  constructor (selector = 'element') {
    super(selector, Element)
    this.type = 'element'
  }

  orphan () {
    const newSelector = isRawTraversal({ instance: this })
      ? `${wrapTraversal({ instance: this })}:orphan`
      : `${this}:orphan`

    return new this._constructor(newSelector)
  }

  traversal ({ arrow: a, connectedElements, connections }) {
    const arrow = toTraversalArrow({ type: a }, { connections }),
          newSelector = isRawTraversal({ instance: this })
            ? `${wrapTraversal({ instance: this })} ${arrow} ${connectedElements}`
            : `${this} ${arrow} ${connectedElements}`

    return new this._constructor(newSelector)
  }
}