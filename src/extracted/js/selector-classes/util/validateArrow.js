export default function validateArrow (arrow) {
  if (!['from', 'to', 'either'].includes(arrow)) {
    throw new Error('Traversal arrow type must be "from", "to", or "either".')
  }
}
