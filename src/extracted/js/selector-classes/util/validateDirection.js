export default function validateDirection (direction) {
  if (!['directed', 'undirected', 'mutual'].includes(direction)) {
    throw new Error('Direction must be "directed", "undirected", or "mutual".')
  }
}