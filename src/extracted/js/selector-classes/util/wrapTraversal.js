export default function wrapTraversal ({ instance }) {
  const Element = instance._constructor
  const wrappedTraversal = (new Element())
    .not({
      selector: (new Element()).not({ selector: instance })
    })
    
  return `${wrappedTraversal}`
}