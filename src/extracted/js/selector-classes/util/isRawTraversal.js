const escapedQuotesRegexp = /\\"/g,
      userInputtedClosedSquareBracketsRegexp = /([^"])(])/g,
      profileFieldNameRegexp = /(\[)("*?")/g,
      profileFieldValueRegexp = /("*?")(\])/g,
      emptyProfileConditionRegexp = /\[.*?\]/g,
      wrappedInPseudoSelectorRegexp = /^(\w+|:)+\(.*?\)(\w+|:)*$/,
      traversalRegexp = /(<---->|<-->|-->|<--)/

export default function isRawTraversal ({ instance }) {
  const withoutUserInput = instance
    .replace(escapedQuotesRegexp, '')
    .replace(userInputtedClosedSquareBracketsRegexp, (match, characterBefore, userInputtedClosedSquareBrackets) => characterBefore)
    .replace(profileFieldNameRegexp, (match, bracket) => bracket)
    .replace(profileFieldValueRegexp, (match, fieldValue, bracket) => bracket)
    .replace(emptyProfileConditionRegexp, '')

  return !wrappedInPseudoSelectorRegexp.test(withoutUserInput) && traversalRegexp.test(withoutUserInput)
}
