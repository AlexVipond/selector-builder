export default function toTraversalArrow ({ type }, options) {
  const connections = options?.connections

  if (!connections) {
    switch (type) {
    case 'from':
      return '<--'
    case 'to':
      return '-->'
    case 'either':
      return '<-->'
    }
  } else {
    switch (type) {
    case 'from':
      return `<--${connections}--`
    case 'to':
      return `--${connections}-->`
    case 'either':
      return `<--${connections}-->`
    }
  }
}
