export default function validateEndpoint (endpoint) {
  if (!['from', 'to'].includes(endpoint)) {
    throw new Error('Endpoint must be "from" or "to".')
  }
}