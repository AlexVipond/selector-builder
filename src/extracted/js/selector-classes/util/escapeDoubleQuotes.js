export default function escapeDoubleQuotes (string) {
  string = string === undefined ? '' : string
  return string.replace(/"/g, '\"')
}