import { isRawTraversal, wrapTraversal, escapeDoubleQuotes } from './util'

export default class Selector extends String {
  constructor (selector = '*', c) {
    super(selector)
    this._constructor = c || Selector
    this.type = 'selector'
  }

  focus () {
    const newSelector = isRawTraversal({ instance: this })
      ? `${wrapTraversal({ instance: this })}:focus`
      : `${this}:focus`

    return new this._constructor(newSelector)
  }

  profile ({ fieldName, operator, fieldValue }) {
    const newSelector = isRawTraversal({ instance: this })
      ? `${wrapTraversal({ instance: this })}["${fieldName.toLowerCase()}" ${operator} "${fieldValue.toLowerCase()}"]`
      : `${this}["${escapeDoubleQuotes(fieldName).toLowerCase()}" ${operator} "${escapeDoubleQuotes(fieldValue).toLowerCase()}"]`

    return new this._constructor(newSelector)
  }

  not ({ selector: opposite }) {
    const newSelector = isRawTraversal({ instance: this })
      ? `${wrapTraversal({ instance: this })}:not(${opposite})`
      : `${this}:not(${opposite})`

    return new this._constructor(newSelector)
  }

  loop (options) {
    const newSelector = isRawTraversal({ instance: this })
      ? `${wrapTraversal({ instance: this })}:loop(${options?.loops || ''})`
      : `${this}:loop(${options?.loops || ''})`
      
    return new this._constructor(newSelector)
  }
}
