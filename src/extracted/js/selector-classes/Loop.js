import Selector from './Selector'

export default class Loop extends Selector {
  constructor (selector = 'loop') {
    super(selector, Loop)
    this.type = 'loop'
  }

  // Loops have no special methods
}
