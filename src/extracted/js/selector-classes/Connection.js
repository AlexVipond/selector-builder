import Selector from './Selector'

export default class Connection extends Selector {
  constructor (selector = 'connection') {
    super(selector, Connection)
    this.type = 'connection'
  }

  direction ({ direction }) {
    const newSelector = `${this}:${direction}`
    
    return new this._constructor(newSelector)
  }

  // Endpoints (from and to)
  endpoint ({ endpoint, elements }) {
    const newSelector = `${this}:${endpoint}(${elements})`
    
    return new this._constructor(newSelector)
  }
}