import { useState } from '@pika/react'

export default function useDefaultConstructedStatus ({ onConstruct, constructedFromStatuses, constructed }) {
  const [defaultConstructedStatus, setDefaultConstructedStatus] = useState('getting')

  switch (defaultConstructedStatus) {
  case 'getting':
    if (constructedFromStatuses.includes('getting')) {
      // Do nothing, still awaiting some default values from child components
    } else {
      setDefaultConstructedStatus('set')
      onConstruct(constructed)
    }
    break
  default:
    // Do nothing; default value has already been set
    break
  }

  return [defaultConstructedStatus, setDefaultConstructedStatus]
}