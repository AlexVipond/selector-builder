import { selectorClasses } from '../json'

export default function reduceConditions ({ selector, conditions }) {
  return (conditions || [])
    .filter(condition => condition?.method)
    .reduce(
      (reducedConditions, { method, argument }) => {
        const withReducedConditions = Object.keys(argument).reduce((withReducedConditions, property) => ({
          ...withReducedConditions,
          [property]: argument[property]?.isUnreducedConditions ? reduceConditions(argument[property]) : argument[property],
        }), {})
        
        return reducedConditions?.[method]?.(withReducedConditions)
      },
      new selectorClasses[selector]()
    )
}