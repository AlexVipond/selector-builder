import { useState } from '@pika/react'

export default function useDefaultValueStatus ({ onInput, optionsMemo, defaultValueMemo, defaultValueFallbackMemo }) {
  const [defaultValueStatus, setDefaultValueStatus] = useState('getting')

  switch (defaultValueStatus) {
  case 'getting':
    setDefaultValueStatus('set')
    onInput({ target: { value: defaultValueMemo || defaultValueFallbackMemo } })
    break
  default:
    // Do nothing; default value has already been set
    break
  }

  return [defaultValueStatus, setDefaultValueStatus]
}