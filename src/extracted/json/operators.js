export default [
  {
    'value': '=',
    'label': 'is'
  },
  {
    'value': '!=',
    'label': 'isn\'t'
  },
  {
    'value': '^=',
    'label': 'starts with'
  },
  {
    'value': '$=',
    'label': 'ends with'
  },
  {
    'value': '*=',
    'label': 'contains'
  },
  {
    'value': '~=',
    'label': 'includes (matches full ids)'
  },
  {
    'value': '>',
    'label': 'greater than'
  },
  {
    'value': '>=',
    'label': 'greater than or equal to'
  },
  {
    'value': '<',
    'label': 'less than'
  },
  {
    'value': '<=',
    'label': 'less than or equal to'
  }
]
