export default [
  {
    'value': 'directed',
    'label': '⟶ directed'
  },
  {
    'value': 'undirected',
    'label': '— undirected'
  },
  {
    'value': 'mutual',
    'label': '⟷ mutual'
  }
]
