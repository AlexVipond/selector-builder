export default [
  {
    'value': 'selector',
    'label': 'All items'
  },
  {
    'value': 'element',
    'label': 'Elements'
  },
  {
    'value': 'connection',
    'label': 'Connections'
  },
  {
    'value': 'loop',
    'label': 'Loops'
  }
]
