export default [
  {
    value: 'from',
    label: 'from ⟵'
  },
  {
    value: 'to',
    label: '⟶ to'
  },
  {
    value: 'either',
    label: 'either ⟷'
  }
]