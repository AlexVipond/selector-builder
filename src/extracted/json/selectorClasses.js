import { Selector, Element, Connection, Loop } from '../js/selector-classes'

export default {
  'selector': Selector,
  'element': Element,
  'connection': Connection,
  'loop': Loop,
}
