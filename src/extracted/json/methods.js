export default [
  {
    value: 'focus',
    label: 'are focus roots',
    requiresArgument: false,
    applicableTo: ['selector', 'element', 'connection', 'loop']
  },
  {
    value: 'profile',
    label: 'have specific data in their profiles',
    requiresArgument: true,
    applicableTo: ['selector', 'element', 'connection', 'loop']
  },
  {
    value: 'not',
    label: 'are not part of a certain group',
    requiresArgument: true,
    applicableTo: ['selector', 'element', 'connection', 'loop']
  },
  {
    value: 'loop',
    label: 'are part of a loop',
    requiresArgument: true,
    applicableTo: ['selector', 'element', 'connection', 'loop']
  },
  {
    value: 'orphan',
    label: 'have no connections',
    requiresArgument: false,
    applicableTo: ['element']
  },
  {
    value: 'traversal',
    label: 'have connections and/or connected elements that meet certain criteria',
    requiresArgument: true,
    applicableTo: ['element']
  },
  {
    value: 'direction',
    label: 'have a specific direction',
    requiresArgument: true,
    applicableTo: ['connection']
  },
  {
    value: 'endpoint',
    label: 'are connected to or from elements that meet certain criteria',
    requiresArgument: true,
    applicableTo: ['connection']
  }
]
