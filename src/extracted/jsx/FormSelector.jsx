import React from '@pika/react'
import { useContext } from '@pika/react'
import { nanoid } from 'nanoid/nanoid'

import { ContextSelector } from '../js'
import { btn, btnRaised } from '../css/composed'

import InterfaceOptionSelector from './InterfaceOptionSelector'
import FormCondition from './FormCondition'
import IconPlus from './IconPlus'

export default function FormSelector ({ enforcedSelector, selector, setSelector, conditions, setConditions, className, label: propsLabel }) {
  const createCondition = () => {
          setConditions([
            ...conditions,
            {
              id: nanoid(),
              method: undefined,
              argument: {},
            },
          ])
        },
        deleteCondition = ({ id }) => {
          const conditionIndex = findConditionIndex({ id, conditions }),
                before = conditionIndex === 0 ? [] : conditions.slice(0, conditionIndex),
                after = conditions.slice(conditionIndex + 1)
          
          setConditions([
            ...before,
            ...after,
          ])
        },
        updateCondition = ({ id, withUpdates }) => {
          const index = findConditionIndex({ id, conditions }),
                item = conditions[index],
                before = index === 0 ? [] : conditions.slice(0, index),
                after = conditions.slice(index + 1)

          setConditions([
            ...before,
            { ...item, ...withUpdates },
            ...after,
          ])
        },
        reorderCondition = ({ id, to }) => {
          const index = findConditionIndex({ id, conditions })
          setConditions(conditions.reorder({ from: index, to }))
        }
  
  let providedContext = {
    selector,
  }

  const receivedContext = useContext(ContextSelector),
        nestingLevel = typeof receivedContext?.nestingLevel === 'number' ? receivedContext.nestingLevel + 1 : 0,
        primaryColor = nestingLevel % 2 === 0 ? 'gray' : 'indigo',
        classesIfNested = nestingLevel > 0 ? `shadow-5 p-4 rounded-5 text-${primaryColor}-90` : 'text-gray-90',
        classesIfNestedAtOddLevel = nestingLevel % 2 === 0 ? 'bg-white' : `bg-${primaryColor}-10`,
        nestingLevelClasses = `${classesIfNested} ${classesIfNestedAtOddLevel}`

  providedContext = {
    ...providedContext,
    nestingLevel,
    primaryColor,
  }

  const label = propsLabel || 'What are you selecting?'

  return (
    <ContextSelector.Provider value={providedContext}>
      <div className={`${className} ${nestingLevelClasses}`}>
        <div className="flex items-center">
          <label class="flex-1 font-6 text-4">{label}</label>
          <InterfaceOptionSelector            
            className="flex-1"
            enforcedSelector={enforcedSelector}
            {...{ selector, setSelector }}
          />
        </div>
        {
          conditions.map((condition, index) => {
            return (
              <div className="mt-4" key={condition.id}>
                <div className="flex items-center">
                  <div className={`w-1/3 ml-auto h-px-1 bg-${primaryColor}-60 rounded-full`} />
                  <span className={`mx-3 uppercase text-3 font-6 tracking-3 text-${primaryColor}-60`}>{index === 0 ? 'that' : 'and'}</span>
                  <div className={`w-1/3 mr-auto h-px-1 bg-${primaryColor}-60 rounded-full`} />
                </div>
                <FormCondition 
                  className="mt-4"
                  condition={condition}
                  updateCondition={updateCondition}
                  deleteCondition={deleteCondition}
                />
              </div>
            )
          })
        }
        <div className="mt-4 flex">
        	<button 
        	  className={`ml-auto ${btn} ${btnRaised} space-x-2 border-indigo-${primaryColor === 'indigo' ? '20' : '10'} bg-indigo-${primaryColor === 'indigo' ? '20' : '10'} text-indigo-90`} 
        	  onClick={createCondition}
        	  type="button"
        	> 
        	  <IconPlus className="h-em-1 w-em-1 stroke-current" />
            <span>Add condition</span>
        	</button>
        </div>
      </div>
    </ContextSelector.Provider>
  )
}


/* Util */
function findConditionIndex ({ id, conditions }) {
  return conditions.findIndex(({ id: i }) => i === id)
}