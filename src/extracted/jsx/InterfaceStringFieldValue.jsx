import React from '@pika/react'

import InterfaceString from './InterfaceString'

export default function InterfaceStringFieldValue ({ fieldValue, setFieldValue, attrs, className }) {
  return (
    <InterfaceString
      {...{
        attrs,
        className,
        value: fieldValue,
        setValue: setFieldValue,
      }}
    />
  )
}