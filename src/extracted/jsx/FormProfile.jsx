import React from '@pika/react'

import InterfaceStringFieldName from './InterfaceStringFieldName'
import InterfaceOptionOperator from './InterfaceOptionOperator'
import InterfaceStringFieldValue from './InterfaceStringFieldValue'

export default function FormProfile ({ profile, setProfile }) {
  const setFieldName = fieldName => setProfile({ ...profile, fieldName }),
        setOperator = operator => setProfile({ ...profile, operator }),
        setFieldValue = fieldValue => setProfile({ ...profile, fieldValue })

  return (
    <div className="mt-2 flex flex-col">
      <InterfaceStringFieldName 
        className="" 
        attrs={{ placeholder: 'Field name' }}
        fieldName={profile.fieldName}
        setFieldName={setFieldName}
      />
      <InterfaceOptionOperator
        className="mt-2"
        operator={profile.operator}
        setOperator={setOperator}
      />
      <InterfaceStringFieldValue
        className="mt-2"
        attrs={{ placeholder: 'Field value' }}
        fieldValue={profile.fieldValue}
        setFieldValue={setFieldValue}
      />
    </div>
  )
}