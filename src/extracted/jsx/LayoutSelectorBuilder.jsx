import React, { useEffect } from '@pika/react'
import { useState, useMemo } from '@pika/react'
import reorderable from '@baleada/logic/factories/reorderable'


import { reduceConditions } from '../js'
import FormSelector from './FormSelector'

export default function LayoutSelectorBuilder () {
  const [selector, setSelector] = useState('selector'),
        [conditions, setConditions] = useState(reorderable([])),
        reducedConditions = useMemo(() => reduceConditions({ selector, conditions }), [selector, conditions])

  useEffect(() => {
    console.log(conditions)
  }, [conditions])

  function setConditionsAsReorderable (array) {
    setConditions(reorderable(array))
  }

  return (
    <main className="min-h-screen w-screen py-12 px-6 flex flex-col items-center justify-center bg-indigo-10">
      <h2 class="w-full max-w-5 uppercase font-6 text-indigo-90 opacity-60 tracking-3 text-3">Selector</h2>
      <pre class="mt-4 rounded-5 shadow-5 p-4 w-full max-w-5 bg-indigo-90 text-indigo-10 overflow-x-scroll"><code>{ `${reducedConditions}` }</code></pre>
      <h1 class="mt-8 w-full max-w-5 uppercase font-6 text-indigo-90 opacity-60 tracking-3 text-3">Selector Builder</h1>
        <form 
          onSubmit={evt => evt.preventDefault()} 
          className="mt-4 bg-white rounded-5 shadow-5 p-7 w-full max-w-5"
        >
          <FormSelector isTopLevel={true} {...{ selector, setSelector, conditions, setConditions: setConditionsAsReorderable }} />
        </form>
    </main>
  )
}
