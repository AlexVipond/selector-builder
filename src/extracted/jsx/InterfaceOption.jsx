import React from '@pika/react'
import { useMemo } from '@pika/react'      

export default function InterfaceOption ({ attrs, className, options, value, setValue }) {
  const fallbackValue = useMemo(() => options[0].value, [options])
  
  function onInput ({ target: { value } }) {
    setValue(value)
  }

  if (value === undefined) {
    setValue(fallbackValue)
  }

  return (
    <select {...attrs} className={`form-select ${className}`} onInput={onInput}>
      {options.map(({ value: v, label }) => <option value={v} selected={v === value}>{label}</option>)}
    </select>
  )
}