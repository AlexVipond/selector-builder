import React from '@pika/react'

import InterfaceOptionDirection from './InterfaceOptionDirection'
import FormSelector from './FormSelector'
import FormProfile from './FormProfile'
import FormEndpoint from './FormEndpoint'
import FormTraversal from './FormTraversal'

export default function FormArgument ({ method, argument, setArgument }) {
  let layout
  switch (method) {
  case 'focus':
    layout = <></>
    break
  case 'profile':
    layout = (
      <FormProfile
        profile={argument}
        setProfile={setArgument}
      />
    )
    break
  case 'not':
    layout = (
      <FormSelector
        label="Which group are they not a part of?"
        className="mt-2"
        selector={argument.selector?.selector}
        setSelector={selector => setArgument({ selector: { ...(argument.selector || {}), selector, isUnreducedConditions: true } })}
        conditions={argument.selector?.conditions || []}
        setConditions={conditions => setArgument({ selector: { ...(argument.selector || {}), conditions, isUnreducedConditions: true } })}
      />
    )
    break
  case 'loop':
    layout = (
      <FormSelector
        label="Specifically,"
        className="mt-2"
        enforcedSelector="loop"
        selector={argument.loops?.selector}
        setSelector={selector => setArgument({ loops: { ...(argument.loops || {}), selector, isUnreducedConditions: true } })}
        conditions={argument.loops?.conditions || []}
        setConditions={conditions => setArgument({ loops: { ...(argument.loops || {}), conditions, isUnreducedConditions: true } })}
      />
    )
    break
  case 'orphan':
    layout = <></>
    break
  case 'traversal':
    layout = (
      <FormTraversal
        traversal={argument}
        setTraversal={setArgument}
      />
    )
    break
  case 'direction':
    layout = (
      <div className="mt-2 flex items-center">
        <InterfaceOptionDirection
          direction={argument.direction}
          setDirection={direction => setArgument({ direction })}
        />
      </div>
    )
    break
  case 'endpoint':
    layout = (
      <FormEndpoint
        endpoint={argument}
        setEndpoint={setArgument}
      />
    )
    break
  default:
    layout = <></>
    break
  }

  return (
    <>{layout}</>
  )
}