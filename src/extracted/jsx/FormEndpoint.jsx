import React from '@pika/react'
import { useMemo } from '@pika/react'

import InterfaceOptionEndpoint from './InterfaceOptionEndpoint'
import FormSelector from './FormSelector'

export default function FormEndpoint ({ endpoint: e, setEndpoint: setE }) {
  const setEndpoint = endpoint => setE({
          ...e,
          endpoint,
        }),
        setElements = elements => setE({
          ...e,
          elements,
        }),
        elementsLabelSuffix = useMemo(() => e.endpoint, [e])
    
  
  return (
    <div className="mt-2 flex flex-col">
      <InterfaceOptionEndpoint
        className=""
        endpoint={e.endpoint}
        setEndpoint={setEndpoint}
      />
      <FormSelector
        className="mt-2"
        label={`Connected ${elementsLabelSuffix}`}
        enforcedSelector="element"
        selector={e.elements?.selector}
        setSelector={selector => setElements({ ...(e.elements || {}), selector, isUnreducedConditions: true })}
        conditions={e.elements?.conditions || []}
        setConditions={conditions => setElements({ ...(e.elements || {}), conditions, isUnreducedConditions: true })}
      />
    </div>
  )
}