import React from '@pika/react'
import { useContext } from '@pika/react'
import { ContextSelector } from '../js'

import { btn, btnRaised } from '../css/composed'

import InterfaceOptionMethod from './InterfaceOptionMethod'
import FormArgument from './FormArgument'
import IconTrash from './IconTrash'

export default function FormCondition ({ condition, updateCondition, deleteCondition, className }) {
  const setMethod = method => {
          updateCondition({
            id: condition.id,
            withUpdates: {
              ...condition,
              method,
            }
          })
        },
        setArgument = argument => {
          updateCondition({
            id: condition.id,
            withUpdates: {
              ...condition,
              argument,
            }
          })
        }

  const { primaryColor } = useContext(ContextSelector)

  return (
    <div class="flex flex-col" className={className}>
      <div className="w-full flex items-center">
        <div className="flex w-full">
          <InterfaceOptionMethod
            className="w-full"
            method={condition.method}
            setMethod={setMethod}
          />
        </div>
        <button 
            className={`ml-3 ${btn} ${btnRaised} all:p-1 border-indigo-${primaryColor === 'indigo' ? '20' : '10'} bg-indigo-${primaryColor === 'indigo' ? '20' : '10'} text-indigo-90`}
            onClick={() => deleteCondition({ id: condition.id })}
            type="button"
          > 
          <span class="sr-only">Remove</span>
          <IconTrash className="h-em-1 w-em-1 stroke-current" />
        </button>
      </div>
      <div className={`ml-2 pl-4 border-l-4 border-${primaryColor}-40`}>
        <FormArgument 
          className=""
          method={condition.method}
          argument={condition.argument}
          setArgument={setArgument}
        />
      </div>
    </div>
  )
}