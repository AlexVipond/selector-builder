import React from '@pika/react'
import { useMemo } from '@pika/react'

import InterfaceOptionArrow from './InterfaceOptionArrow'
import FormSelector from './FormSelector'

export default function FormTraversal ({ traversal, setTraversal }) {
  const setArrow = arrow => setTraversal({
          ...traversal,
          arrow
        }),
        setConnections = connections => setTraversal({
          ...traversal,
          connections,
        }),
        setConnectedElements = connectedElements => setTraversal({
          ...traversal,
          connectedElements,
        }),
        connectedElementsLabelSuffix = useMemo(() => {
          switch (traversal.arrow) {
          case 'from':
            return 'from'
          case 'to':
            return 'to'
          case 'either':
            return 'from or to'
          default:
            return ' '
          }
        }, [traversal])
    
      
  
  return (
    <div className="mt-2 flex flex-col">
      <InterfaceOptionArrow
        className=""
        arrow={traversal.arrow}
        setArrow={setArrow}
      />
      <FormSelector
        className="mt-2"
        label={`Connected ${connectedElementsLabelSuffix}`}
        enforcedSelector="element"
        selector={traversal.connectedElements?.selector}
        setSelector={selector => setConnectedElements({ ...(traversal.connectedElements || {}), selector, isUnreducedConditions: true })}
        conditions={traversal.connectedElements?.conditions || []}
        setConditions={conditions => setConnectedElements({ ...(traversal.connectedElements || {}), conditions, isUnreducedConditions: true })}
      />
      <FormSelector
        className="mt-2"
        label="Via"
        enforcedSelector="connection"
        selector={traversal.connections?.selector}
        setSelector={selector => setConnections({ ...(traversal.connections || {}), selector, isUnreducedConditions: true })}
        conditions={traversal.connections?.conditions || []}
        setConditions={conditions => setConnections({ ...(traversal.connections || {}), conditions, isUnreducedConditions: true })}
      />
    </div>
  )
}