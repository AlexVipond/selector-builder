import React from '@pika/react'
import { useEffect } from '@pika/react'
import { useDefaultValueStatus} from '../js'

export default function InterfaceString ({ value, setValue, attrs, className }) {
  const fallbackValue = ''
  
  function onInput ({ target: { value } }) {
    setValue(value)
  }

  if (value === undefined) {
    setValue(fallbackValue)
  }

  return (
    <input {...attrs} type="text" className={`form-input ${className}`} onInput={onInput} value={value} />
  )
}