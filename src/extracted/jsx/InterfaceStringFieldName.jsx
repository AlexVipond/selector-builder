import React from '@pika/react'

import InterfaceString from './InterfaceString'

export default function InterfaceStringFieldName ({ fieldName, setFieldName, attrs, className }) {
  return (
    <InterfaceString
      {...{
        attrs,
        className,
        value: fieldName,
        setValue: setFieldName,
      }}
    />
  )
}