import React from '@pika/react'

import { arrows } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionArrow ({ arrow, setArrow, attrs, className }) {
  const options = arrows

  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: arrow,
        setValue: setArrow,
      }}
    />
  )
}