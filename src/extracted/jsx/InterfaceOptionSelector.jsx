import React from '@pika/react'

import { selectors } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionSelector ({ attrs, className, enforcedSelector, selector, setSelector }) {
  const options = enforcedSelector
          ? selectors.filter(({ value: v }) => v === enforcedSelector)
          : selectors
          
  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: selector,
        setValue: setSelector,
      }}
    />
  )
}