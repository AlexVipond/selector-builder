import React from '@pika/react'

export default function IconPlus ({ className }) {
  return (
    <svg className={className} fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor"
      viewBox="0 0 24 24">
      <path d="M12 4v16m8-8H4"></path>
    </svg>
  )
}