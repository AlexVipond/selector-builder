import React from 'react'

export default function FormFooter () {
  return (
    <footer className="px-3 py-8 bg-gray-90 text-gray-50 text-2">
      <div className="flex flex-col">
        <div className="mt-4 md:mt-0 flex flex-col md:flex-row md:items-center">
          {/* Spacer */}
          <div className="md:hidden mt-4 mx-auto w-11 h-px-1 rounded-full bg-gray-60"></div>
          <div className="mt-4 md:mt-0 flex-1 flex flex-col items-center md:items-end justify-center md:border-r border-gray-60">
            <a
              className="hover:text-gray-20"
              rel="noopener"
              href="https://gitlab.com/alexvipond/selector-builder"
            >
              <span>View source code</span>
              {/* <SimpleGitLab className={'block fill-current h-7 w-7'} /> */}
            </a>
            <a
              className="ml-4 hover:text-gray-20"
              rel="noopener"
              href="https://kumu.io/join"
            >
              <span>Get a free Kumu account!</span>
              {/* <SimpleTwitter className={'block fill-current h-7 w-7'} /> */}
            </a>
          </div>
          {/* Spacer */}
          <div className="mt-4 md:mt-0 mx-auto md:mx-4 w-11 h-px-1 md:w-px-1 md:h-em-3/2 rounded-full bg-gray-60"></div>
          <div className="mt-7 md:mt-0 flex-1 flex flex-col items-center justify-center md:items-start">
            <span className="">
              Created by <a className="underline hover:text-gray-20" href="https://alexvipond.dev">Alex Vipond</a> 🌱for <a className="underline hover:text-gray-20" href="https://kumu.io">Kumu</a>
            </span>
          </div>
        </div>
      </div>
    </footer>
  )
}
