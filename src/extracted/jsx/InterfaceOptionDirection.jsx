import React from '@pika/react'

import { directions } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionDirection ({ direction, setDirection, attrs, className }) {
  const options = directions

  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: direction,
        setValue: setDirection,
      }}
    />
  )
}