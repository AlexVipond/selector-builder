import React from '@pika/react'

import { endpoints } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionEndpoint ({ endpoint, setEndpoint, attrs, className }) {
  const options = endpoints

  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: endpoint,
        setValue: setEndpoint,
      }}
    />
  )
}