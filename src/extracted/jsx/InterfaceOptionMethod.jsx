import React from '@pika/react'
import { useContext, useMemo } from '@pika/react'

import { ContextSelector } from '../js'
import { methods } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionMethod ({ method, setMethod, attrs, className }) {
  const { selector } = useContext(ContextSelector),
        options = useMemo(
          () => methods.filter(({ applicableTo }) => applicableTo.includes(selector)),
          [selector]
        )

  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: method,
        setValue: setMethod,
      }}
    />
  )
}