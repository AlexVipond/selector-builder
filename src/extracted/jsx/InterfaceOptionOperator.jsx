import React from '@pika/react'

import { operators } from '../json'
import InterfaceOption from './InterfaceOption'

export default function InterfaceOptionOperator ({ operator, setOperator, attrs, className }) {
  const options = operators

  return (
    <InterfaceOption
      {...{
        attrs,
        className,
        options,
        value: operator,
        setValue: setOperator,
      }}
    />
  )
}