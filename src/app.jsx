import React from '@pika/react'
import ReactDOM from '@pika/react-dom'
import './extracted/css/tailwind.css'

import { LayoutSelectorBuilder, LayoutFooter } from './extracted/jsx'

function App () {
  return (
    <>
      <LayoutSelectorBuilder />
      <LayoutFooter />
    </>
  )
}

ReactDOM.render(
  <React.StrictMode><App /></React.StrictMode>, document.getElementById('app')
)