import { Loop, Selector } from '../src/extracted/js/selector-classes'

it('extends the base Selector class', () => {
  const loop = new Loop(),
        result = loop instanceof Selector

  expect(result).toBe(true)
})

it('stores Kumu selector type', () => {
  const loop = new Loop()
  expect(loop.type).toBe('loop')
})

it('defaults to loop', () => {
  const loop = new Loop()
  expect(`${loop}`).toBe('loop')
})

it('can method chain', () => {
  const loop = new Loop(),
        result = loop
          .focus()
          .not({
            selector: new Loop(),
          })

  expect(result instanceof Loop).toBe(true)
})