import { Selector, Loop } from '../src/extracted/js/selector-classes'

it('extends the String prototype', () => {
  const selector = new Selector(),
        result = selector instanceof String

  expect(result).toBe(true)
})

it('stores Kumu selector type', () => {
  const selector = new Selector()
  expect(selector.type).toBe('selector')
})

it('defaults to *', () => {
  const selector = new Selector()
  expect(`${selector}`).toBe('*')
})

it('selects focus roots', () => {
  const selector = new Selector(),
        result = selector.focus()
  expect(`${result}`).toBe('*:focus')
})

it('selects based on profile data', () => {
  const selector = new Selector(),
        result = selector.profile({
          fieldName: 'label',
          operator: '=',
          fieldValue: 'Kumu'
        })

  expect(`${result}`).toBe('*["label" = "kumu"]')
})

it('excludes items from selection (using :not())', () => {
  const selector = new Selector(),
        result = selector.not({ selector: (new Selector()).focus() })
  expect(`${result}`).toBe('*:not(*:focus)')
})

it('selects loops and everything within them', () => {
  const selector = new Selector(),
        result = selector.loop()
  expect(`${result}`).toBe('*:loop()')
})

it('selects everything within a specific loop', () => {
  const selector = new Selector(),
        result = selector.loop({ loops: new Loop() })
  expect(`${result}`).toBe('*:loop(loop)')
})

it('can method chain', () => {
  const selector = new Selector(),
        result = selector
          .focus()
          .profile({ fieldName: 'My Field', operator: '=', fieldValue: 'My Value' })
          .not({
            selector: (new Selector()).profile({ fieldName: 'My Field', operator: '=', fieldValue: 'Not My Value' })
          })
          .loop()
          
  expect(result instanceof Selector).toBe(true)
})