import { Connection, Element, Selector } from '../src/extracted/js/selector-classes'

it('extends the base Selector class', () => {
  const connection = new Connection(),
        result = connection instanceof Selector

  expect(result).toBe(true)
})

it('stores Kumu selector type', () => {
  const connection = new Connection()
  expect(connection.type).toBe('connection')
})

it('defaults to connection', () => {
  const connection = new Connection()
  expect(`${connection}`).toBe('connection')
})

it('selects based on direction', () => {
  const connection = new Connection(),
        directed = connection.direction({ direction: 'directed' }),
        undirected = connection.direction({ direction: 'undirected' }),
        mutual = connection.direction({ direction: 'mutual' }),
        result = [`${directed}`, `${undirected}`, `${mutual}`],
        expected = ['connection:directed', 'connection:undirected', 'connection:mutual']

  expect(result).toStrictEqual(expected)
})

it('selects based on endpoints', () => {
  const connection = new Connection(),
        from = connection.endpoint({ endpoint: 'from', elements: new Element() }),
        to = connection.endpoint({ endpoint: 'to', elements: new Element() }),
        result = [
          `${from}`,
          `${to}`,
        ],
        expected = [
          'connection:from(element)',
          'connection:to(element)'
        ]

  expect(result).toStrictEqual(expected)
})

it('can method chain', () => {
  const connection = new Connection(),
        result = connection
          .direction({ direction: 'directed' })
          .endpoint({
            endpoint: 'from',
            elements: new Element(),
          })

  expect(result instanceof Connection).toBe(true)
})
