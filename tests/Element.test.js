import { Element, Connection, Selector } from '../src/extracted/js/selector-classes'

it('extends the base Selector class', () => {
  const element = new Element(),
        result = element instanceof Selector

  expect(result).toBe(true)
})

it('stores Kumu selector type', () => {
  const element = new Element()
  expect(element.type).toBe('element')
})

it('defaults to element', () => {
  const element = new Element()
  expect(`${element}`).toBe('element')
})

it('selects orphans', () => {
  const element = new Element(),
        result = element.orphan()
  expect(`${result}`).toBe('element:orphan')
})

it('selects based on profile data of other elements', () => {
  const element = new Element(),
        anyFrom = element.traversal({
          arrow: 'from',
          connectedElements: new Element(),
        }),
        someFrom = element.traversal({
          arrow: 'from',
          connectedElements: new Element(),
          connections: new Connection(),
        }),
        anyTo = element.traversal({
          arrow: 'to',
          connectedElements: new Element(),
        }),
        someTo = element.traversal({
          arrow: 'to',
          connectedElements: new Element(),
          connections: new Connection(),
        }),
        anyEither = element.traversal({
          arrow: 'either',
          connectedElements: new Element(),
        }),
        someEither = element.traversal({
          arrow: 'either',
          connectedElements: new Element(),
          connections: new Connection(),
        }),
        result = [
          `${anyFrom}`,
          `${someFrom}`,
          `${anyTo}`,
          `${someTo}`,
          `${anyEither}`,
          `${someEither}`,
        ],
        expected = [
          'element <-- element',
          'element <--connection-- element',
          'element --> element',
          'element --connection--> element',
          'element <--> element',
          'element <--connection--> element',
        ]
  
  expect(result).toStrictEqual(expected)
})

it('can method chain', () => {
  const element = new Element(),
        result = element
          .orphan()
          .traversal({
            arrow: 'from',
            connectedElements: new Element(),
          })

  expect(result instanceof Element).toBe(true)
})