import { isRawTraversal } from '../src/extracted/js/selector-classes/util'

// isRawTraversal only uses native String methods internally, so these instances don't have to be actual constructed Selector instances

it('handles the simplest cases', () => {
  const instance = '*'
  expect(isRawTraversal({ instance })).toBe(false)
})

it('handles simple profile data selectors', () => {
  const instance = 'element["my field" = "my value"]'
  expect(isRawTraversal({ instance })).toBe(false)
})

it('handles simple arrow examples', () => {
  const from = 'element <-- element',
        to = 'element --> element',
        either = 'element <--> element',
        result = [
          isRawTraversal({ instance: from }),
          isRawTraversal({ instance: to }),
          isRawTraversal({ instance: either }),
        ],
        expected = [
          true,
          true,
          true,
        ]

  expect(result).toStrictEqual(expected)
})

it('handles traversals inside pseudo-selectors', () => {
  const instance = 'element:not(element --> element):focus'
  expect(isRawTraversal({ instance })).toBe(false)
})

it('handles arrows inside profile data selectors', () => {
  const instance = 'element["my field -->" = "my <--> value"]'
  expect(isRawTraversal({ instance })).toBe(false)
})

it('handles arrows and escaped quotes and closed square brackets inside profile data selectors', () => {
  const instance = 'element["break --> \\"israwtraversal"="\\"my value\\" in quotes ] with square brackets thrown in"]'
  expect(isRawTraversal({ instance })).toBe(false)
})

it('handles escaped quotes with adjacent closed square brackets inside profile data selectors', () => {
  const instance = 'element["break --> \\"israwtraversal"="\\"my value\\"]"]'
  expect(isRawTraversal({ instance })).toBe(false)
})
