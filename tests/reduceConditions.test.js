import reduceConditions from '../src/extracted/js/reduceConditions'
import { Selector, Loop } from '../src/extracted/js/selector-classes'

it('recursively reduces conditions', () => {
  const selector = 'selector',
        conditions = [
          {
            method: 'profile',
            argument: {
              fieldName: 'Label',
              operator: '=',
              fieldValue: 'Kumu',
            }
          },
          {
            method: 'loop',
            argument: {
              selector: {
                isUnreducedConditions: true, // The isUnreducedConditions boolean tells reduceConditions to recurse, passing this object as the argument
                selector: 'loop',
                conditions: [
                  {
                    method: 'profile',
                    argument: {
                      fieldName: 'Label',
                      operator: '=',
                      fieldValue: 'My Loop',
                    }
                  }
                ]
              }
            }
          }
        ],
        result = reduceConditions({ selector, conditions })

  expect(`${result}`).toBe('*["label" = "kumu"]:loop(loop["label" = "my loop"])')
})
