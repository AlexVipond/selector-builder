import { Element } from '../src/extracted/js/selector-classes'
import { wrapTraversal } from '../src/extracted/js/selector-classes/util'

it('wraps traversals in double element:not() pseudo-selectors' , () => {
  const traversal = (new Element()).traversal({
          arrow: 'from',
          connectedElements: new Element(),
        }),
        result = wrapTraversal({ instance: traversal })

  expect(`${result}`).toBe('element:not(element:not(element <-- element))')
})