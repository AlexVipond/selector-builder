import { Selector, Element, Loop } from '../src/extracted/js/selector-classes'
import { wrapTraversal } from '../src/extracted/js/selector-classes/util'

const traversalStub = (new Element()).traversal({
        arrow: 'from',
        connectedElements: new Element()
      }),
      wrappedTraversalStub = wrapTraversal({ instance: traversalStub })


it('selects focus roots', () => {
  const traversal = traversalStub,
        result = traversal.focus()
  expect(`${result}`).toBe(`${wrappedTraversalStub}:focus`)
})

it('selects based on profile data', () => {
  const traversal = traversalStub,
        result = traversal.profile({
          fieldName: 'label',
          operator: '=',
          fieldValue: 'Kumu'
        })

  expect(`${result}`).toBe(`${wrappedTraversalStub}["label" = "kumu"]`)
})

it('excludes items from selection (using :not())', () => {
  const traversal = traversalStub,
        result = traversal.not({ selector: (new Selector()).focus() })
  expect(`${result}`).toBe(`${wrappedTraversalStub}:not(*:focus)`)
})

it('selects loops and everything within them', () => {
  const traversal = traversalStub,
        result = traversal.loop()
  expect(`${result}`).toBe(`${wrappedTraversalStub}:loop()`)
})

it('selects everything within a specific loop', () => {
  const traversal = traversalStub,
        result = traversal.loop({ selector: new Loop() })
  expect(`${result}`).toBe(`${wrappedTraversalStub}:loop(loop)`)
})

it('selects orphans', () => {
  const traversal = traversalStub,
        result = traversal.orphan()
  expect(`${result}`).toBe(`${wrappedTraversalStub}:orphan`)
})

it('selects based on profile data of other elements', () => {
  const traversal = traversalStub,
        result = traversal.traversal({
          arrow: 'from',
          connectedElements: new Element()
        })
  
  expect(`${result}`).toBe(`${wrappedTraversalStub} <-- element`)
})