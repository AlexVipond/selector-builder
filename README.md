# Selector Builder

For local development:

```
npm install
```

```
npm run dev
```

Edit files in the `src` directory.

Selector classes are stored in `src/extracted/js/selector-classes`. They're tested with Jest:

```
npm run test
```